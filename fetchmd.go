package metabookr

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
)

const OpenLibURI = "https://openlibrary.org/search.json"

// Docs is a struct for the docs key returned by the OpenLibrary API
type Docs struct {
	Key                    string   `json:"key"`
	Type                   string   `json:"type"`
	Seed                   []string `json:"seed"`
	Title                  string   `json:"title"`
	Title_suggest          string   `json:"title_suggest"`
	Has_fulltext           bool     `json:"has_fulltext"`
	Edition_count          int      `json:"edition_count"`
	Edition_key            []string `json:"edition_key"`
	Publish_date           []string `json:"publish_date"`
	Publish_year           []int    `json:"publish_year"`
	First_publish_year     int      `json:"first_publish_year"`
	Number_of_pages_median int      `json:"number_of_pages_median"`
	ISBN                   []string `json:"isbn"`
	Last_modified_i        int      `json:"last_modified_i"`
	Ebook_count_i          int      `json:"ebook_count_i"`
	Cover_edition_key      string   `json:"cover_edition_i"`
	Cover_i                int      `json:"cover_i"`
	Publisher              []string `json:"publisher"`
	Language               []string `json:"language"`
	Author_key             []string `json:"author_key"`
	Author_name            []string `json:"author_name"`
	Id_goodreads           []string `json:"id_goodreads"`
	Id_librarything        []string `json:"id_librarything"`
	Publisher_facet        []string `json:"publisher_facet"`
	Version                int      `json:"_version_"`
	Author_facet           []string `json:"author_facet"`
}

// OLResponse is a struct for the response returned by the OpenLibrary API
type OLResponse struct {
	NumFound      int    `json:"numFound"`
	Start         int    `json:"start"`
	NumFoundExact bool   `json:"numFoundExact"`
	Docs          []Docs `json:"docs"`
}

// AskOpenLibrary sends the title to Open Librarie's API and
// reads the json response and unmarshals it into olresponse.
func AskOpenLibrary(title string) (OLResponse, error) {
	var olresponse OLResponse
	searchquery := "?title=" + title

	wclient := http.Client{
		Timeout: time.Second * 5,
	}

	req, err := http.NewRequest(http.MethodGet, OpenLibURI+searchquery, nil)
	if err != nil {
		return olresponse, err
	}

	req.Header.Set("User-Agent", "metabookr/v0.1.0")

	res, getErr := wclient.Do(req)
	if getErr != nil {
		return olresponse, err
	}

	if res.Body != nil {
		defer res.Body.Close()
	}

	oljson, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return olresponse, err
	}

	err = json.Unmarshal([]byte(oljson), &olresponse)

	return olresponse, err
}
