package main

import (
	"codeberg.org/steffenfritz/metabookr"
	flag "github.com/spf13/pflag"
	"log"
	"os"
	"path/filepath"
)

func main() {
	rootDir := flag.StringP("input-dir", "i", "", "The root directory to scan.")
	outputFile := flag.StringP("output-file", "o", "output", "The output file name.")
	flag.Parse()

	var olresponses []metabookr.OLResponse

	err := filepath.Walk(*rootDir, func(path string, fileName os.FileInfo, err error) error {
		if err != nil {
			log.Println(err.Error())
		}
		if !fileName.IsDir() {

			normtitle := metabookr.TrimSuffix(fileName.Name())
			normtitle, wsReplaced := metabookr.ReplaceWS(normtitle)
			normtitle, usReplaced := metabookr.ReplaceUnderscore(normtitle)
			if !usReplaced && !wsReplaced {
				normtitle = metabookr.ReplaceCamelCase(normtitle)
			}
			println(normtitle)

			olresponse, err := metabookr.AskOpenLibrary(normtitle)
			if err != nil {
				log.Printf("Error: %s", err.Error())
			}

			// Don't add results with more than 5 hits. This can happen if title too generic.
			if olresponse.NumFound < 6 {
				olresponses = append(olresponses, olresponse)
			}

		}

		return nil
	})

	if err != nil {
		log.Fatalln(err)
	}

	err = metabookr.WriteOLTSV(*outputFile, olresponses)
	if err != nil {
		log.Println(err)
	}
}
