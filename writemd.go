package metabookr

import (
	"encoding/csv"
	"io"
	"log"
	"os"
	"strconv"
)

// NewTSVWriter creates a new TSV writer
func NewTSVWriter(w io.Writer) (writer *csv.Writer) {
	writer = csv.NewWriter(w)
	writer.Comma = '\t'

	return
}

// WriteOLTSV writes OLResponses to a tsv file
func WriteOLTSV(outputFile string, responses []OLResponse) error {
	if len(responses) == 0 {
		log.Println("No metadata found.")
		return nil
	}

	fd, err := os.Create(outputFile + ".tsv")
	if err != nil {
		return err
	}
	defer fd.Close()

	tsv := NewTSVWriter(fd)

	for _, response := range responses {
		if response.NumFound > 0 {
			for _, doc := range response.Docs {
				for _, isbn := range doc.ISBN {
					err = tsv.Write([]string{doc.Title, isbn, strconv.Itoa(doc.First_publish_year)})
					if err != nil {
						log.Println(err)
					}
				}
			}
		}
	}

	tsv.Flush()
	return nil
}
