package metabookr

import "strings"

// CommonSuffixes contains some ebook suffixes
var CommonSuffixes = []string{".cbr",
	".cbz",
	".cb7",
	".epub",
	".mobi",
	".pdf",
	".txt",
	".zip"}

// TrimSuffix removes some common file suffixes
func TrimSuffix(fileName string) string {
	fileNameTrim := ""
	for _, suf := range CommonSuffixes {
		if strings.HasSuffix(fileName, suf) {
			fileNameTrim = strings.TrimSuffix(fileName, suf)
		}
	}

	return fileNameTrim
}
