module codeberg.org/steffenfritz/metabookr

go 1.17

require (
	github.com/fatih/camelcase v1.0.0
	github.com/spf13/pflag v1.0.5
)
