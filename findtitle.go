package metabookr

import (
	"github.com/fatih/camelcase"
	"strings"
)

// ReplaceUnderscore reads a string and replaces all
// underscores with "+" and returns the string
func ReplaceUnderscore(fileName string) (string, bool) {
	if strings.Contains(fileName, "_") {
		return strings.ReplaceAll(fileName, "_", "+"), true
	}

	return fileName, false
}

// ReplaceCamelCase reads a string and adds a "+"
// after each capital letter and returns the string
func ReplaceCamelCase(fileName string) string {
	splitted := camelcase.Split(fileName)
	splittedFileName := ""

	for _, t := range splitted {
		splittedFileName += t + "+"
	}

	return strings.TrimRight(splittedFileName, "+")
}

// ReplaceWS reads a string and if it has white spaces
// it replaces them with "+" and return the string
func ReplaceWS(fileName string) (string, bool) {
	if strings.Contains(fileName, " ") {
		return strings.ReplaceAll(fileName, " ", "+"), true
	}

	return fileName, false
}
